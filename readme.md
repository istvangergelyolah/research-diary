Kutatási napló specifikáció
============

**Fejlesztők**
| Név | Neptun | Email |
|-|-|-|
| Frendl Péter | HY01LZ | pfre00@gmail.com |
| Oláh István Gergely | HN02A6 | olah.istvan.gergely@gmail.com | 


## Rövid specifikáció
A kutatók napi több kísérletet végeznek, melyek eredményeit érdemes feljegyezni,
követni. A hallgatók feladata egy online kutatási napló készítése, amely ezek
követését és megosztását elősegíti.

### Követelmények
* háromszintű felhasználókezelés: látogató, regisztrált user, admin
* kísérletek: egy tulajdonos és 0 vagy több további résztvevő, a kísérlet lehet nyilvános vagy privát
* a kísérlethez tartozik szöveg, ábra, egyéb fájl (eredménytáblázat), url
* több kísérlet egy nagyobb projektbe fogható, amelynek szintén van
* tulajdonosa, résztvevői pedig a kísérletek résztvevői
* a látogató látja a nyilvános kísérleteket
* a regisztrált felhasználó adhat hozzá új kísérletet, de projektet nem. Lehet tulajdonos, illetve résztvevő.
* az admin adhat hozzá új projektet, illetve törölhet mindent

## Technológia és architektúra
A megvalósított alkalmazás egy webes felülettel rendelkező Java Spring project lesz, MYSQL adatbázissal.

## Specifikáció
A specifikáció alapján nem szükséges a látogatónak szerepelnie a modellben.
Alapvetően egy CRUD alkalmazással dolgozunk, így elkészítettem egy táblázatot a
főbb view-ek számára.
| Entitás  | Create  | Read | Update | Delete |
|:-:|:-:|:-:|:-:|:-:|
|**felhasználó** | regisztráció | profil és userlista | profil (magunknak) | profil és lista (adminnak/magunknak) |
| **admin** | szerverindító oldal | profil és userlista | profil (magunknak) | profil és lista (magunknak/adminnak) |
| **kísérlet** | új kísérlet | kísérlet vagy kísérletlista (public/private) | kísérlet (public/private) |  kísérlet vagy kísérletlista (magunknak/adminnak) | 
| **projekt** | <i class="icon-add"></i>új projekt | projekt vagy projektlista (public/private) | projekt (jogosultnak) | projekt (jogosultnak)  | 


###Felmerült kérdések

| Kérdés | Válasz |
|:-|:-|
| Első admint hogy hozzuk létre? |Szerver első indításakor látható oldalon|
| Elfelejtett jelszóval kell törődni? | Email-kiküldése|
| Last-admin probléma?| Utolsó admint ne tudjuk törölni |
| Hogy foglalunk kísérleteket projectbe? Önkényesen bárkit fel vagy levehetünk? | Inkább a projekttulajok vehessenek fel kísérleteket. Az bekerülhet, hogy a kísérlet tulaja jelentkezik a projektbe és a projekttulaj engedélyezi.|
| User törlés esetén ha kísérlet/projekt tulajdonosa, mi lesz azzal?| Az admin új tulajdonoshoz rendelheti. |
| Tulajdonos adhat hozzá résztvevőket, résztvevők nem, csak editálhatják a kísérletet (leírás, fájlfeltöltés)? | Igen, Igen. Résztvevő leiratkozhat.|
| Ha résztvevő feltölthet fájlokat, akkor törölhet is?| Csak a sajátjait. |
| Kísérlethez tartozó ember is módosíthatja a projektoldalt?| Nem, csak a projekthez közvetlenül tartozó. |
| Mi tartozik a projekthez? Leírás, kapcsolódó kísérletek, tagok listája? Lehet feltölteni fájlokat? | Lehet. |
|Létezhet olyan személy, aki résztvevője a projektnek, de kísérletnek nem? | Igen, pl. szakmai felügyelő. |

## Funkciók

### Jogkörök
* **Látogató**:  Nincs bejegyzett fiókja a rendszerben. Látja a publikus kísérleteket, felhasználói fiókokat.
* **Regisztrált user**: Létrehozhat kísérletet, regisztrálhat kísérletekre. Törölheti magát a rendszerből. Lejelentkezhet kísérletekről.
* **Admin**:  Létrehozhat, módosíthat törölhet kísérletet, projektet, felhasználót. Lejelentkezhet projektről, kísérletről. 

## Főbb view-ek és funkciók

#### Login
Login index oldal bejelentkezésre. Azonosításra emailt vagy nevet lehet használni, jelszóval.  Hibás bejelentkezés esetén hibát ír ki. Tartalmaz linket a regisztráció és az 'elfelejtett jelszó' oldalra.

#### Elfelejtett jelszó oldal
Egy szövegmezőt tartalmaz - az email-cím számára és egy küldés-gombot, illetve egy linket a login oldalra.

#### Regisztráció
Oldal felhasználó regisztrációra. Név, email és jelszó szükséges a megfelelő formai követelményekkel. Tartalmaz egy linket a login oldalra. Sikeres regisztrációnál átirányít oda.

#### Fejléc
Navigációs fejléc. Felsorolja a többi (elérhető) oldalra a linkeket és tartalmaz egy kijelentkezés gombot (ha bejelentkezett felhasználó).

#### Értesítések
A felhasználóhoz tartozó friss értesítéseket tartalmazza linkekkel, dátum szerint csökkenő sorrendben, mint frissült kísérlet-oldal. Egy felhasználóhoz akkor tartozik egy értesítés, ha résztvevő vagy tulajdonos adott kísérletben vagy projektben.

#### Profil
Regisztrált felhasználó vagy admin személyes adatait (név, email) tartalmazó oldal.  Admin és a saját profiloldal esetén ezek módosíthatóak, illetve megjelenik egy *'Felhasználó törlése'* gomb. Ha résztvevő, a hozzá kapcsolódó projektekre/kísérletekre mutatnak itt linkek. 

#### Új kísérlet
Regisztrált felhasználóval a új kísérletet tudunk létrehozni az ennek megfelelő oldalon. Kötelezően kitöltendő az egyedi név és egy leírás.

#### Kísérlet
Meglévő kísérlethez tartozó oldal, névvel és leírással. Fel vannak sorolva (linkelve) a résztvevők és a tulajdonos. Ha projekt alá van szervezve, tartalmaz linket rá. Ha nincs tulajdonosa, egy gomb jelenik meg, amivel adoptálható a kísérlet. Tulajdonos és admin esetén a résztvevők eltávolíthatóak, újak vehetőek fel, a meglévő mezők módosíthatóak.
Résztvevők esetén fájlokat tölthetünk fel és törölhetjük a sajátunkat. A fájlok feltöltéséről egy táblázat készül, pontos dátummal és névvel ellátva.

#### Új projekt
Adminnal új projektet tudunk létrehozni az ennek megfelelő oldalon. Kötelezően kitöltendő az egyedi név és egy leírás.

#### Projekt
Meglévő projekthez tartozó oldal, névvel és leírással. Fel vannak sorolva (linkelve) a hozzá tartozó kísérletek, a közvetlen résztvevők és a tulajdonos. Ha nincs tulajdonosa, egy gomb jelenik meg, amivel adoptálható a projekt. Tulajdonos és admin esetén a közvetlen résztvevők és kísérletek eltávolíthatóak, újak vehetőek fel, a meglévő mezők módosíthatóak.
A közvetlen résztvevők esetén fájlokat tölthetünk fel és törölhetjük a sajátunkat. A fájlok feltöltéséről egy táblázat készül, pontos dátummal és névvel ellátva.

#### Felhasználó-lista
Táblázatos formában ABC-be sorrendezve tartalmazza a felhasználókat név, email szerint. A lista szűrhető.

#### Kísérlet-lista
Táblázatos formában ABC-be sorrendezve tartalmazza a kísérleteket név, tulajdonos és dátum szerint. A lista szűrhető.

#### Projekt-lista
Táblázatos formában ABC-be sorrendezve tartalmazza a projekteket név, tulajdonos és dátum szerint. A lista szűrhető.

![](https://s21.postimg.org/5hk9zmllz/usecase.png)

[TOC]