var Schema = require('mongoose').Schema;
var db = require('../config/db');

var Experiment = db.model('Experiment', {
    name: String,
    description: String,
    created: { type: Date, default: Date.now },
    private: { type: Boolean, default: false },
    _owner: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    _members: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    _files: [{
        type: Schema.Types.ObjectId,
        ref: 'File'
    }],
    _project: {
        type: Schema.Types.ObjectId,
        ref: 'Project'
    }
});

module.exports = Experiment;