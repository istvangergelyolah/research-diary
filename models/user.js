var Schema = require('mongoose').Schema;
var db = require('../config/db');

var User = db.model('User', {
    name: String,
    email: String,
    password: String, // TODO hash!
    description: String,
    token: String,
    tokenExpire: { type: Date },
    admin: { type: Boolean, default: false },
    lastLogin: { type: Date, default: Date.now },

});

module.exports = User;