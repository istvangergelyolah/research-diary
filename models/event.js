/**
 * User model (mock)
 * @constructor
 */
var Event = function() {};

/**
 * An instance
 * @type {{id: number, name: string, email: string, pass: string}}
 */
var EventInstanceMock = {
    id: 1,
    name: 'New Project',
    source: '/projects/132212',
    desc: 'New project for some science stuff',
    date: '2016.10.10 13:30'
};

/**
 * Find one element with the criteria
 * @param criteria
 * @param cb error first callback
 * @returns {*}
 */
Event.prototype.findOne = function(criteria, cb) {

    //returns 1 mocked item
    return cb(null, EventInstanceMock);
};

/**
 * Find all elements with the criteria
 * @param criteria
 * @param cb error first callback
 * @returns {*}
 */
Event.prototype.find = function(criteria, cb) {

    //returns 3 mocked item
    return cb(null, [EventInstanceMock, EventInstanceMock, EventInstanceMock]);
};

/**
 * Save the item to the db
 * @param cb error first callback
 * @returns {*}
 */
Event.prototype.save = function(cb) {
    return cb(null, this);
};

module.exports = Event;