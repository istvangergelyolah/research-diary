var Schema = require('mongoose').Schema;
var db = require('../config/db');


var File = db.model('File', {
    name: String,
    type: String,
    size: Number,
    uploaded: { type: Date, default: Date.now },
    _link: {
        type: Schema.Types.ObjectId,
        ref: 'fs.files'
    },
    _uploader: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = File;