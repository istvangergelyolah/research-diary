var Schema = require('mongoose').Schema;
var db = require('../config/db');

var Project = db.model('Project', {
    name: String,
    description: String,
    _owner: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    _members: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    _files: [{
        type: Schema.Types.ObjectId,
        ref: 'File'
    }],
    created: { type: Date, default: Date.now },
});

module.exports = Project;