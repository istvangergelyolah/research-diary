/**
 * Crud and more advanced management for functions
 *
 */

var authMW = require('../middleware/generic/auth');
var renderMW = require('../middleware/generic/render');
var getEventsMW = require('../middleware/event/getUserEvents');
var getUserMW = require('../middleware/user/getUser');

// models

var userModel = require('../models/user');
//var eventModel = require('../models/event');

module.exports = function(app) {

    var objectRepository = {
        eventModel: [
            { 'id': 23, 'name': 'New Project: XXYZ', 'desc': 'lorem ipsum', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 24, 'name': 'XXYZ Project Deleted', 'desc': 'lorem lodem', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 25, 'name': 'XX Project updated', 'desc': 'iimimimimimimimi', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
        ],
    };

    // get functions list
    app.get("/content/events",
        authMW(objectRepository),
        getEventsMW(objectRepository),
        renderMW(objectRepository, 'content/events')
    );


}