// import middlewares
var mainRedirectMW = require('../middleware/generic/mainredirect');
var authMW = require('../middleware/generic/auth');
var renderMW = require('../middleware/generic/render');

module.exports = function(app) {

    var objectRepository = {
        eventModel: [
            { 'id': 23, 'name': 'New Project: XXYZ', 'desc': 'lorem ipsum', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 24, 'name': 'XXYZ Project Deleted', 'desc': 'lorem lodem', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 25, 'name': 'XX Project updated', 'desc': 'iimimimimimimimi', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
        ],
    };

    app.get("/events",
        authMW(objectRepository),
        renderMW(objectRepository, 'overview')
    );

    app.get("/projects",
        renderMW(objectRepository, 'overview')
    );

    app.get("/experiments",
        renderMW(objectRepository, 'overview')
    );

    app.get("/addexperiment",
        authMW(objectRepository),
        renderMW(objectRepository, 'overview')
    );

    app.get("/addproject",
        authMW(objectRepository),
        renderMW(objectRepository, 'overview')
    );

    app.get("/editprofile",
        authMW(objectRepository),
        renderMW(objectRepository, 'overview')
    );

    app.get("/users",
        renderMW(objectRepository, 'overview')
    );

}