/**
 * Crud and more advanced management for functions
 *
 */

var authMW = require('../middleware/generic/auth');
var renderMW = require('../middleware/generic/render');

var getFileMW = require('../middleware/generic/getFile')

var fileModel = require('../models/file');

module.exports = function(app) {

    var objectRepository = {
        fileModel: fileModel,
        eventModel: [
            { 'id': 23, 'name': 'New Project: XXYZ', 'desc': 'lorem ipsum', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 24, 'name': 'XXYZ Project Deleted', 'desc': 'lorem lodem', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 25, 'name': 'XX Project updated', 'desc': 'iimimimimimimimi', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
        ]

    };

    app.param("fileID",
        getFileMW(objectRepository)
    );

    app.get("/file/:fileID",
        function(req, res, next) {
            res.tpl.file.readstream.pipe(res);
        }
    );
}