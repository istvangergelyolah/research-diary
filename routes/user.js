/**
 * This routing supports user management - like CRUD operations
 * and login, logout
 */

// import middlewares
var authMW = require('../middleware/generic/auth');
var inverseAuthMW = require('../middleware/generic/inverseauth');
var renderMW = require('../middleware/generic/render');
var loginWithTokenMW = require('../middleware/user/loginWithToken');

var getUserMW = require('../middleware/user/getUser');
var getUsersMW = require('../middleware/user/getUsers');
var setUserTokenMW = require('../middleware/user/setUserToken');

var getUserExperimentsMW = require('../middleware/experiment/getUserExperiments')
var getUserProjectsMW = require('../middleware/project/getUserProjects')
var getCurrentUserMW = require('../middleware/user/getCurrentUser');
var updateUserMW = require('../middleware/user/updateUser');
var checkUserParametersMW = require('../middleware/user/checkUserParameters');
var deleteUserMW = require('../middleware/user/deleteUser');

var logoutUserMW = require('../middleware/user/logoutUser');
var checkUserLoginMW = require('../middleware/user/checkUserLogin');

var sendMailMW = require('../middleware/generic/sendmail');


// models
var userModel = require('../models/user');
var experimentModel = require('../models/experiment');
var projectModel = require('../models/project');

module.exports = function(app) {

    var objectRepository = {
        userModel: userModel,
        experimentModel: experimentModel,
        projectModel: projectModel,
        eventModel: [
            { 'id': 23, 'name': 'New Project: XXYZ', 'desc': 'lorem ipsum', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 24, 'name': 'XXYZ Project Deleted', 'desc': 'lorem lodem', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 25, 'name': 'XX Project updated', 'desc': 'iimimimimimimimi', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
        ]
    };

    // <LOGOUT LOGIN REGISTER DELETE>

    // login
    app.post("/login",
        inverseAuthMW(objectRepository),
        checkUserLoginMW(objectRepository),
        renderMW(objectRepository, 'login')
    );

    // simple logout, redirects if logged out already
    app.post("/logout",
        authMW(objectRepository),
        logoutUserMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/');
        }
    );

    // register a new user
    // error handling needed
    app.post("/user",
        inverseAuthMW(objectRepository),
        checkUserParametersMW(objectRepository),
        renderMW(objectRepository, 'register')
    );

    // user delete. needs redirect
    // use post instead of delete to support html 4 verion forms and auto redirection
    app.post("/user/delete",
        authMW(objectRepository),
        deleteUserMW(objectRepository),
        logoutUserMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/');
        }
    );

    // </LOGOUT LOGIN REGISTER DELETE>

    // <USER PROFILE SETTINGS>

    app.get("/user/editprofile",
        authMW(objectRepository),
        renderMW(objectRepository, 'editprofile')
    );

    // edit current user profile - TODO generalize
    app.get("/content/editprofile",
        authMW(objectRepository),
        getCurrentUserMW(objectRepository),
        getUserExperimentsMW(objectRepository),
        getUserProjectsMW(objectRepository),
        renderMW(objectRepository, 'content/editprofile')
    );

    // </USER PROFILE SETTINGS>

    // update current users profile
    app.post("/user/editprofile",
        authMW(objectRepository),
        getCurrentUserMW(objectRepository),
        updateUserMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/');
        }
    );

    // get user list
    app.get("/content/users",
        getUsersMW(objectRepository),
        renderMW(objectRepository, 'content/users')
    );

    app.param("userID",
        getUserMW(objectRepository)
    );

    app.get("/content/user/:userID",
        getUserExperimentsMW(objectRepository),
        getUserProjectsMW(objectRepository),
        renderMW(objectRepository, 'content/user')
    );

    app.get("/user/:userID",
        renderMW(objectRepository, 'overview')
    );

    app.param("tokenID",
        function(req, res, next, id) {
            res.tpl.token = id;
            return next();
        }
    );

    app.get("/reset/:userID/:tokenID",
        loginWithTokenMW(objectRepository),
        renderMW(objectRepository, 'overview')
    );

    app.post("/forgottenpassword",
        setUserTokenMW(objectRepository),
        function(req, res, next) {

            // create mail
            var mailOptions = {
                from: "researcherdiary@gmail.com",
                to: res.tpl.user.email,
                subject: "Forgotten password",
                text: 'You can reset your password here if you want: http://' + req.headers.host + '/reset/' + res.tpl.user._id + '/' + res.tpl.user.token
            }
            res.tpl.user = {};
            res.tpl.mail = mailOptions;
            return next();
        },
        sendMailMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/');
        }
    );

}