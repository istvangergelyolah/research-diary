/**
 * Crud and more advanced management for functions
 *
 */

var authMW = require('../middleware/generic/auth');
var renderMW = require('../middleware/generic/render');
var getExperimentsMW = require('../middleware/experiment/getExperiments');
var getExperimentMW = require('../middleware/experiment/getExperiment');


var assignUserToExperimentMW = require('../middleware/experiment/assignUserToExperiment');
var resignUserFromExperimentMW = require('../middleware/experiment/resignUserFromExperiment');

var getUsersMW = require('../middleware/user/getUsers');
var deleteExperimentMW = require('../middleware/experiment/deleteExperiment');
var createExperimentMW = require('../middleware/experiment/createExperiment');
var getUserMW = require('../middleware/user/getUser');

var saveFileMV = require('../middleware/generic/saveFile')
var getFileMW = require('../middleware/generic/getFile')
var removeFileMW = require('../middleware/generic/removeFile')
    // models

var userModel = require('../models/user');
var fileModel = require('../models/file');
var experimentModel = require('../models/experiment');

module.exports = function(app) {

    var objectRepository = {
        experimentModel: experimentModel,
        userModel: userModel,
        fileModel: fileModel,
        eventModel: [
            { 'id': 23, 'name': 'New Project: XXYZ', 'desc': 'lorem ipsum', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 24, 'name': 'XXYZ Project Deleted', 'desc': 'lorem lodem', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 25, 'name': 'XX Project updated', 'desc': 'iimimimimimimimi', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
        ]

    };

    app.param("experimentID",
        getExperimentMW(objectRepository)
    );

    app.param("userID",
        getUsersMW(objectRepository)
    );

    // get functions list
    app.get("/content/addexperiment",
        authMW(objectRepository),
        renderMW(objectRepository, 'content/addexperiment')
    );


    // get experiment list
    app.get("/content/experiments",
        getExperimentsMW(objectRepository),
        function(req, res, next) {
            console.log(res.tpl)
            if (typeof(req.session.userid) == 'undefined') {
                // filter out private projects
                res.tpl.experiments = res.tpl.experiments.filter(function(experiment) {
                    return (!experiment.private);
                })
            }
            return next();
        },
        renderMW(objectRepository, 'content/experiments')
    );

    app.get("/content/experiment/:experimentID",
        getUsersMW(objectRepository),
        function(req, res, next) {
            // filter user list
            res.tpl.users = res.tpl.users.filter(function(obj) {
                if (res.tpl.experiment._owner._id.toString() == obj._id.toString()) {
                    return false;
                }

                for (var memberid in res.tpl.experiment._members) {
                    if (res.tpl.experiment._members[memberid]._id.toString() == obj._id.toString()) {
                        return false;
                    }
                }
                return true;
            });
            return next();
        },
        renderMW(objectRepository, 'content/experiment')
    );

    app.get("/experiment/:experimentID",
        renderMW(objectRepository, 'overview')
    );

    app.post("/experiment",
        authMW(objectRepository),
        createExperimentMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/experiments'); // unused redirection
        }
    );

    app.post("/experiment/:experimentID/assign/:userID",
        authMW(objectRepository),
        assignUserToExperimentMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/experiments'); // unused redirection
        }
    );

    app.post("/experiment/:experimentID/resign/:userID",
        authMW(objectRepository),
        resignUserFromExperimentMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/experiments'); // unused redirection
        }
    );


    // delete experiment
    app.post("/experiment/:experimentID/delete",
        authMW(objectRepository),
        deleteExperimentMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/experiments');
        }
    );

    // upload file
    app.post("/experiment/:experimentID/file",
        authMW(objectRepository),
        function(req, res, next) {
            res.tpl.targetEntity = res.tpl.experiment;
            return next();
        },
        saveFileMV(objectRepository),
        function(req, res, next) {
            return res.redirect('/experiments');
        }
    );

    app.param("fileID",
        getFileMW(objectRepository)
    );


    app.post("/experiment/:experimentID/file/:fileID/delete",
        function(req, res, next) {
            res.tpl.targetEntity = res.tpl.experiment;
            return next();
        },
        removeFileMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/experiments');
        }
    );

    app.param("permissionLevel",
        function(req, res, next, permissionLevel) {
            res.tpl.permissionLevel = permissionLevel;
            return next();
        }
    );


    app.post("/experiment/:experimentID/permission/:permissionLevel",
        function(req, res, next) {
            if (res.tpl.permissionLevel.toLowerCase() == "private") {
                console.log("!")
                private = true;
            }
            if (res.tpl.permissionLevel.toLowerCase() == "public") {
                private = false;
            }
            if (typeof(private) != 'undefined') {
                console.log("!++" + private)
                res.tpl.experiment.private = private;
                res.tpl.experiment.save();
            }
            return res.redirect('/experiments');
        }
    );

}