/**
 * Crud and more advanced management for functions
 *
 */

var authMW = require('../middleware/generic/auth');
var renderMW = require('../middleware/generic/render');
var getprojectsMW = require('../middleware/project/getProjects');
var getprojectMW = require('../middleware/project/getProject');


var assignUserToprojectMW = require('../middleware/project/assignUserToProject');
var resignUserFromprojectMW = require('../middleware/project/resignUserFromProject');

var addExperimentToProjectMW = require('../middleware/project/addExperimentToProject');
var removeExperimentFromProjectMW = require('../middleware/project/removeExperimentFromProject');

var getExperimentMW = require('../middleware/experiment/getExperiment')
var getExperimentsMW = require('../middleware/experiment/getExperiments')

var getUsersMW = require('../middleware/user/getUsers');
var deleteprojectMW = require('../middleware/project/deleteProject');
var createprojectMW = require('../middleware/project/createProject');
var getUserMW = require('../middleware/user/getUser');

var getProjectExperimentsMW = require('../middleware/experiment/getProjectExperiments');

var saveFileMV = require('../middleware/generic/saveFile')
var getFileMW = require('../middleware/generic/getFile')
var removeFileMW = require('../middleware/generic/removeFile')
    // models

var userModel = require('../models/user');
var projectModel = require('../models/project');
var fileModel = require('../models/file');
var experimentModel = require('../models/experiment');

module.exports = function(app) {

    var objectRepository = {
        projectModel: projectModel,
        userModel: userModel,
        experimentModel: experimentModel,
        fileModel: fileModel,
        eventModel: [
            { 'id': 23, 'name': 'New Project: XXYZ', 'desc': 'lorem ipsum', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 24, 'name': 'XXYZ Project Deleted', 'desc': 'lorem lodem', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
            { 'id': 25, 'name': 'XX Project updated', 'desc': 'iimimimimimimimi', 'source': '/projects/132212', 'date': '2016.10.20 13:30' },
        ]

    };

    app.param("projectID",
        getprojectMW(objectRepository)
    );

    app.param("userID",
        getUsersMW(objectRepository)
    );

    app.param("experimentID",
        getExperimentMW(objectRepository)
    );

    // get functions list
    app.get("/content/addproject",
        authMW(objectRepository),
        renderMW(objectRepository, 'content/addproject')
    );


    // get project list
    app.get("/content/projects",
        getprojectsMW(objectRepository),
        renderMW(objectRepository, 'content/projects')
    );

    app.get("/content/project/:projectID",
        getUsersMW(objectRepository),
        getExperimentsMW(objectRepository),
        getProjectExperimentsMW(objectRepository),
        function(req, res, next) {
            // filter user list
            res.tpl.users = res.tpl.users.filter(function(obj) {
                if (res.tpl.project._owner._id.toString() == obj._id.toString()) {
                    return false;
                }

                for (var memberid in res.tpl.project._members) {
                    if (res.tpl.project._members[memberid]._id.toString() == obj._id.toString()) {
                        return false;
                    }
                }
                return true;
            });
            return next();
        },
        function(req, res, next) {
            // filter experiment list
            res.tpl.experiments = res.tpl.experiments.filter(function(obj) {
                for (var experimentid in res.tpl.project._experiments) {
                    if (res.tpl.project._experiments[experimentid]._id.toString() == obj._id.toString()) {
                        return false;
                    }
                }
                return true;
            });
            return next();
        },
        renderMW(objectRepository, 'content/project')
    );

    app.get("/project/:projectID",
        renderMW(objectRepository, 'overview')
    );

    app.post("/project",
        authMW(objectRepository),
        createprojectMW(objectRepository),
        renderMW(objectRepository, 'content/projects')
    );

    app.post("/project/:projectID/assign/:userID",
        authMW(objectRepository),
        assignUserToprojectMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/projects'); // unused redirection
        }
    );

    app.post("/project/:projectID/resign/:userID",
        authMW(objectRepository),
        resignUserFromprojectMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/projects'); // unused redirection
        }
    );


    // delete project
    app.post("/project/:projectID/delete",
        authMW(objectRepository),
        deleteprojectMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/projects');
        }
    );

    app.post("/project/:projectID/add/:experimentID",
        authMW(objectRepository),
        addExperimentToProjectMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/projects'); // unused redirection
        }
    );

    app.post("/project/:projectID/remove/:experimentID",
        authMW(objectRepository),
        removeExperimentFromProjectMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/projects'); // unused redirection
        }
    );

    // upload file
    app.post("/project/:projectID/file",
        authMW(objectRepository),
        function(req, res, next) {
            res.tpl.targetEntity = res.tpl.project;
            return next();
        },
        saveFileMV(objectRepository),
        function(req, res, next) {
            return res.redirect('/projects');
        }
    );

    app.param("fileID",
        getFileMW(objectRepository)
    );


    app.post("/project/:projectID/file/:fileID/delete",
        function(req, res, next) {
            res.tpl.targetEntity = res.tpl.project;
            return next();
        },
        removeFileMW(objectRepository),
        function(req, res, next) {
            return res.redirect('/projects');
        }
    );

}