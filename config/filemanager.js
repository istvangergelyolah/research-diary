var mongoose = require('./db');

var Schema = mongoose.Schema;
var conn = mongoose.connection;

var fs = require('fs');
var GridStream = require('gridfs-stream');
GridStream.mongo = mongoose.mongo;
var gfs = new GridStream(conn.db);

module.exports = gfs;