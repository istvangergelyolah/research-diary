var gfs = require('../../config/filemanager');
var mongoose = require('../../config/db');
var formidable = require('formidable');
var fs = require("fs");
var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {

    var fileModel = requireOption(objectRepo, 'fileModel');

    return function(req, res, next) {
        var id = res.tpl.file.id;
        gfs.remove({ _id: id }, function(err) {
            if (err) return handleError(err);
            // success
            res.tpl.targetEntity._files = res.tpl.targetEntity._files.filter(function(file) {
                if (file._link.toString() == id.toString()) {
                    fileModel.remove({
                        _id: file._id
                    }, function(err, result) {
                        // return next();
                    });
                }
                return file._link.toString() != id.toString()
            })
            res.tpl.targetEntity.save();
        });

        return next();
    };

};