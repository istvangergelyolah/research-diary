var gfs = require('../../config/filemanager');
var mongoose = require('../../config/db');
var formidable = require('formidable');
var fs = require("fs");
var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {

    return function(req, res, next, id) {

        var readstream = gfs.createReadStream({
            _id: id
        });

        //error handling, e.g. file does not exist
        readstream.on('error', function(err) {
            console.log('An error occurred!', err);
            throw err;
        });
        res.tpl.file = {};
        res.tpl.file.id = id;
        res.tpl.file.readstream = readstream;
        // readstream.pipe(res);
        return next();
    };

};