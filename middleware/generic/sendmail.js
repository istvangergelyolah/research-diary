var transport = require('../../config/mailer');

module.exports = function(objectRepo) {

    return function(req, res, next) {

        if (typeof res.tpl.mail == 'undefined') {
            return res.redirect("/");
        }

        transport.sendMail(res.tpl.mail, function(error, response) {
            if (error) {
                console.log(error);
            }
        });

        return next();
    };

};