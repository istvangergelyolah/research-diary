/**
 * Using the template engine render the values into the template
 * Note: copied from mintahazi2015
 */
module.exports = function(objectRepo, viewName) {
    return function(req, res) {
        for (var index in objectRepo) {
            res.tpl[index] = objectRepo[index];
        }
        return res.render(viewName, res.tpl);
    };
}