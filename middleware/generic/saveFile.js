var gfs = require('../../config/filemanager');
var mongoose = require('../../config/db');
var formidable = require('formidable');
var fs = require("fs");
var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {

    var fileModel = requireOption(objectRepo, 'fileModel');

    return function(req, res, next) {

        var form = new formidable.IncomingForm();
        form.multiples = false;
        form.on('file', function(field, file) {

            //  var gfs = conn.gfs;
            var writestream = gfs.createWriteStream({
                filename: file.name,
            });
            fs.createReadStream(file.path).pipe(writestream);

            var newFile = new fileModel();
            newFile._uploader = req.session.userid;
            newFile.name = file.name;
            newFile.size = file._writeStream.bytesWritten;
            newFile._link = writestream.id;
            newFile.save();

            res.tpl.targetEntity._files.push(newFile.id);
            res.tpl.targetEntity.save();
        });

        // once all the files have been uploaded, send a response to the client
        form.on('end', function() {

        });

        form.parse(req);
        return next();
    };

};