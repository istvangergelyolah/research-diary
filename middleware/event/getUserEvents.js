/**
 * Get the list of the users events
 *
 */
var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {
    var eventModel = requireOption(objectRepo, 'eventModel');

    return function(req, res, next) {
        return next();
    };

};