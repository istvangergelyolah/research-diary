/**
 * Returns with the list of the experiments
 *
 */
var requireOption = require('../generic/require').requireOption;
var url = require('url');

module.exports = function(objectrepository) {
    var experimentModel = requireOption(objectrepository, 'experimentModel');

    return function(req, res, next) {

        var queryData = url.parse(req.url, true).query;


        if (queryData.abc !== undefined) {
            experimentModel.find({}).sort('name').populate('_owner').exec(function(err, result) {
                res.tpl.experiments = result;
                next();
            });
        } else if (queryData.latest !== undefined) {
            experimentModel.find({}).sort('-date').populate('_owner').exec(function(err, result) {
                res.tpl.experiments = result;
                next();
            });
        } else {
            //lets find the table
            var querystring = {};
            if (queryData.name !== undefined) {
                querystring = ({ name: { $regex: queryData.name, $options: 'i' } });
            }

            experimentModel.find(querystring).populate('_owner')
                .exec(function(err, result) {
                    res.tpl.experiments = result;
                    next();
                });
        }
    };

};