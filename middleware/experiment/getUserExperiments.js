/**
 * Get the list of the users events
 *
 */
var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {
    var experimentModel = requireOption(objectRepo, 'experimentModel');

    return function(req, res, next) {
            //lets find the experiment
        experimentModel.find({
                _owner: res.tpl.user._id
            }).populate('_members')
            .exec(function(err, result) {

                res.tpl.experiments = result;
                return next();
            });

    };

};