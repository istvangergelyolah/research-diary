/**
 * Delete the experiment
 *
 */

var requireOption = require('../generic/require').requireOption;

module.exports = function(objectrepository) {

    var experimentModel = requireOption(objectrepository, 'experimentModel');

    return function(req, res, next) {
        //lets find the experiment
        experimentModel.remove({
            _id: res.tpl.experiment._id
        }, function(err, result) {
            return next();
        });
    };

};