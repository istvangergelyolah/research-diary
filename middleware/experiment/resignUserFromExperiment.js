/**
 * Update fucntion with the given ID and content
 *
 */

var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {

    return function(req, res, next) {

        res.tpl.experiment._members = res.tpl.experiment._members.filter(function(member) {
            return member._id.toString() != res.tpl.user._id.toString()
        })
        res.tpl.experiment.save();
        return next();
    };

};