/**
 * Update fucntion with the given ID and content
 *
 */

var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {

    var experimentModel = requireOption(objectRepo, 'experimentModel');

    return function(req, res, next) {
        //not enough parameter
        if ((typeof req.body === 'undefined') || (typeof req.body.name === 'undefined') ||
            (typeof req.body.description === 'undefined')) {
            return next();
        }
        if (typeof(req.session.userid) == 'undefined') {
            res.tpl.error.push('Please log in before trying to create or modify anything.');
            return next();
        }

        // all neccessary parameters are present
        // validate them

        //lets find the experimennt
        experimentModel.findOne({
            name: req.body.name
        }, function(err, result) {

            if ((err) || (result !== null)) {
                res.tpl.error.push('Already resolved experiment name. Choose another name');
                return next();
            }
            // here can be validated the code
            //create user
            var newExperiment = new experimentModel();
            newExperiment.name = req.body.name;
            newExperiment.description = req.body.description;
            newExperiment._owner = req.session.userid;
            newExperiment.save(function(err) {
                return next();
            });
        });
    };

};