/**
 * Returns with the list of the users
 *
 */
var requireOption = require('../generic/require').requireOption;
var url = require('url');

module.exports = function(objectrepository) {
    var userModel = requireOption(objectrepository, 'userModel');

    return function(req, res, next) {

        var queryData = url.parse(req.url, true).query;


        if (queryData.abc !== undefined) {
            userModel.find({}).sort('name').exec(function(err, result) {
                res.tpl.users = result;
                next();
            });
        } else if (queryData.latest !== undefined) {
            userModel.find({}).sort('-date').exec(function(err, result) {
                res.tpl.users = result;
                next();
            });
        } else {
            //lets find the table
            var querystring = {};
            if (queryData.name !== undefined) {
                querystring = ({ name: { $regex: queryData.name, $options: 'i' } });
            }

            userModel.find(querystring, function(err, result) {
                res.tpl.users = result;
                next();
            });
        }
    };

};