/**
 * This module gets an user with an ID.
 * if ID not provided, returns with the logged in user
 *
 */
var requireOption = require('../generic/require').requireOption;

module.exports = function(objectrepository) {

    var userModel = requireOption(objectrepository, 'userModel');

    return function(req, res, next) {

        //lets find the user
        userModel.findOne({
            email: req.body.email
        }, function(err, result) {
            result.token = Math.random().toString(36).substring(2, 32);
            result.tokenExpire = Date.now() + 3600000; // one hour ahead
            result.save();
            res.tpl.user = result;
            return next();
        });

    };
};