module.exports = function(objectRepo) {

    return function(req, res, next) {

        result = res.tpl.user;
        if (res.tpl.token.toString() != "" && res.tpl.token.toString() == result.token.toString() && Date.now() < result.tokenExpire) {
            result.lastLogin = Date.now();
            result.token = "";
            result.save();
            req.session.userid = result._id;
            req.session.name = result.name;
            res.tpl.name = result.name;
            //redirect to / so the app can decide where to go next
            return res.redirect('/events');
        }
        return next();
    };

};