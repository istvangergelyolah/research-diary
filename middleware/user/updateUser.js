/**
 * Updates an existing user or creates a new one
 *
 */


var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {

    var UserModel = requireOption(objectRepo, 'userModel');

    return function(req, res, next) {
        //not enough parameter
        if (typeof res.tpl.user === 'undefined') {
            res.tpl.error.push('User not found!');
            return next();
        }

        user = res.tpl.user;
            // update name
        if (typeof req.body.name != 'undefined' && req.body.name != '') {
            user.name = req.body.name;
            // update the session name too!
            req.session.name = req.body.name;
        }
        if (typeof req.body.password != 'undefined' && req.body.password != '' && req.body.password == req.body.password2) {
            user.password = req.body.password; // TODO HASH
        }

        if (typeof req.body.description != 'undefined' && req.body.description != '') {
            user.description = req.body.description;
        }

        user.save();
        return next();
    };

};