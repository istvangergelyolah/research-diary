/**
 * ugly copypaste - TODO refactor
 *
 */
var requireOption = require('../generic/require').requireOption;

module.exports = function(objectrepository) {

    var userModel = requireOption(objectrepository, 'userModel');

    return function(req, res, next) {
        userModel.findOne({
            _id: req.session.userid
        }, function(err, result) {
            res.tpl.user = result;
            return next();
        });

    };
};