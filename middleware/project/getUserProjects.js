/**
 * Get the list of the users events
 *
 */
var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {
    var projectModel = requireOption(objectRepo, 'projectModel');

    return function(req, res, next) {
        //lets find the project
        projectModel.find({
                _owner: res.tpl.user._id
            }).populate('_members')
            .exec(function(err, result) {
                console.log(result);
                res.tpl.projects = result;
                return next();
            });

    };

};