/**
 * Update fucntion with the given ID and content
 *
 */

var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {



    return function(req, res, next) {
        if (res.tpl.project._members.indexOf(res.tpl.user._id) == -1) {
            res.tpl.project._members.push(res.tpl.user);
            res.tpl.project.save();
        }

        return next();
    };

};