/**
 * Returns with the list of the projects
 *
 */
var requireOption = require('../generic/require').requireOption;
var url = require('url');

module.exports = function(objectrepository) {
    var projectModel = requireOption(objectrepository, 'projectModel');

    return function(req, res, next) {

        var queryData = url.parse(req.url, true).query;


        if (queryData.abc !== undefined) {
            projectModel.find({}).sort('name').populate('_owner').exec(function(err, result) {
                res.tpl.projects = result;
                next();
            });
        } else if (queryData.latest !== undefined) {
            projectModel.find({}).sort('-date').populate('_owner').exec(function(err, result) {
                res.tpl.projects = result;
                next();
            });
        } else {
            //lets find the table
            var querystring = {};
            if (queryData.name !== undefined) {
                querystring = ({ name: { $regex: queryData.name, $options: 'i' } });
            }

            projectModel.find(querystring).populate('_owner')
                .exec(function(err, result) {
                    res.tpl.projects = result;
                    next();
                });
        }
    };

};