/**
 * Get the list of the users events
 *
 */
var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {
    var projectModel = requireOption(objectRepo, 'projectModel');

    return function(req, res, next, id) {
        //lets find the project
        projectModel.findOne({
            _id: id
        }).populate('_members').populate('_owner').populate({
            path: '_files',
            populate: { path: '_uploader' }
        }).exec(function(err, result) {
            res.tpl.project = result;
            res.tpl.owner = false;
            if (req.session.userid && result._owner._id.toString() == req.session.userid.toString()) {
                res.tpl.owner = true;
            }
            res.tpl.member = false;
            for (var member in result._members) {
                if (req.session.userid && result._members[member]._id.toString() == req.session.userid.toString()) {
                    res.tpl.member = true;
                    break;
                }
            }
            return next();
        });

    };

};