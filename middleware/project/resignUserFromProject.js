/**
 * Update fucntion with the given ID and content
 *
 */

var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {

    return function(req, res, next) {

        res.tpl.project._members = res.tpl.project._members.filter(function(member) {
            return member._id.toString() != res.tpl.user._id.toString()
        })
        res.tpl.project.save();
        return next();
    };

};