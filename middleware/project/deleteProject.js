/**
 * Delete the project
 *
 */

var requireOption = require('../generic/require').requireOption;

module.exports = function(objectrepository) {

    var projectModel = requireOption(objectrepository, 'projectModel');

    return function(req, res, next) {
        //lets find the project
        projectModel.remove({
            _id: res.tpl.project._id
        }, function(err, result) {
            return next();
        });
    };

};