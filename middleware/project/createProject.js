/**
 * Update fucntion with the given ID and content
 *
 */

var requireOption = require('../generic/require').requireOption;

module.exports = function(objectRepo) {

    var projectModel = requireOption(objectRepo, 'projectModel');

    return function(req, res, next) {
        //not enough parameter
        if ((typeof req.body === 'undefined') || (typeof req.body.name === 'undefined') ||
            (typeof req.body.description === 'undefined')) {
            return next();
        }


        if (typeof(req.session.userid) == 'undefined') {
            res.tpl.error.push('Please log in before trying to create or modify anything.');
            return next();
        }
        // all neccessary parameters are present
        // validate them

        //lets find the experimennt
        projectModel.findOne({
            name: req.body.name
        }, function(err, result) {

            if ((err) || (result !== null)) {
                res.tpl.error.push('Already resolved project name. Choose another name');
                return next();
            }
            // here can be validated the code
            //create user
            var newProject = new projectModel();
            newProject.name = req.body.name;
            newProject.description = req.body.description;
            newProject._owner = req.session.userid;
            newProject.save(function(err) {
                //redirect to /login
                return next();
            });
        });
    };

};