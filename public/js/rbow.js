$(function() {
    loadPage();
});

function loadPage(urlPath) {
    var path = "";
    if (typeof(urlPath) == 'undefined') {
        // get from urlPath
        var currentPage = document.URL;
        relativeURL = currentPage.replace(/^(?:\/\/|[^\/]+)*\//, "");
        path = "/content/" + relativeURL;
    } else {
        // valid path

        path = urlPath;
    }
    console.log("LOADPAGE: " + path)
        // set callbacks for menu
    wireButtons();
    $("#content").load(path, wireContentButtons);
}

function wireContentButtons() {

    $('.experiment_view_btn').on("click", function() {
        history.pushState('', 'Title', "/experiment/" + $(this).attr('data-id'));
        $("#content").load("/content/experiment/" + $(this).attr('data-id'), function() {
            wireContentButtons();
        });

    });

    $('.project_view_btn').on("click", function() {
        history.pushState('', 'Title', "/project/" + $(this).attr('data-id'));
        $("#content").load("/content/project/" + $(this).attr('data-id'), function() {
            wireContentButtons();
        });

    });

    $('.user_view_btn').on("click", function() {
        history.pushState('', 'Title', "/user/" + $(this).attr('data-id'));
        $("#content").load("/content/user/" + $(this).attr('data-id'), function() {
            wireContentButtons();
        });

    });
}

function wireButtons() {

    // callbacks for menu buttons
    $("#logout_btn").on("click", function() {
        $("#logoutform").submit();
    });

    $(".popup .close").on("click", function() {
        $(".popup").hide();
    });

    $('html').on("click", function() {
        $("#eventbox").fadeOut("fast", "swing");
    });

    $("#eventbox").on("click", function(event) {
        event.stopPropagation();
    });

    $('#eventbox_btn').on("click", function(event) {
        event.stopPropagation();
        $("#eventbox").fadeToggle("fast", "swing");
    });

    $('#events_btn').on("click", function() {
        $("#content").load("/content/events", function(response, status, xhr) {});
        history.pushState('', 'Title', '/events');
        $("#eventbox").fadeOut("fast", "swing");
    });

    $('#users_btn').on("click", function() {
        $("#content").load("/content/users", function(response, status, xhr) {
            wireContentButtons();
        });
        history.pushState('', 'Title', '/users');
    });

    $('#addexperiment_btn').on("click", function() {
        $("#content").load("/content/addexperiment", function(response, status, xhr) {
            wireContentButtons();
        });
        history.pushState('', 'Title', '/addexperiment');
    });

    $('#addproject_btn').on("click", function() {
        $("#content").load("/content/addproject", function(response, status, xhr) {
            wireContentButtons();
        });
        history.pushState('', 'Title', '/addproject');
    });


    $('#projects_btn').on("click", function() {
        $("#content").load("/content/projects", function(response, status, xhr) {
            wireContentButtons();
        });
        history.pushState('', 'Title', '/projects');
    });

    $('#experiments_btn').on("click", function() {
        $("#content").load("/content/experiments", function(response, status, xhr) {
            wireContentButtons();
        });
        history.pushState('', 'Title', '/experiments');
    });

    $('#profile_btn').on("click", function() {
        $("#content").load("/content/editprofile", function(response, status, xhr) {
            wireContentButtons();
        });
        history.pushState('', 'Title', '/editprofile');
    });
}